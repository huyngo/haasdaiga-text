# haasdaiga-text

Hàäsdáïga text translations:

- Schneewittchen (German fairy tale)
- Le Petit Prince (French novella)
- The Legend of Saint Gióng (Vietnamese legend)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

The original works are in the public domain.
