# Text information

- Source language: German
- Retrieved from: [Grimm's Fairy Tales][grimm], with English translation
- Original work status: Public domain

[grimm]: https://www.grimmstories.com/language.php?grimm=053&l=en&r=de
