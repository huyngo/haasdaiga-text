# Text information

- Source language: French
- Retrieved from: [Project Gutenberg Australia][gutenberg]
- Status: Public domain in Australia (whence I retrieved the text), France
    (original author's country), and Vietnam (my country)

[gutenberg]: http://gutenberg.net.au/ebooks03/0300771h.html
