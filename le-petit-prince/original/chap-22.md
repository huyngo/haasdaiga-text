# CHAPITRE XXII

-Bonjour, dit le petit prince.

-Bonjour, dit l'aiguilleur.

-Que fais-tu ici? dit le petit prince.

-Je trie les voyageurs, par paquets de mille, dit l'aiguilleur.
J'expédie les trains qui les emportent, tantôt vers la droite, tantôt
vers la gauche.

Et un rapide illuminé, grondant comme le tonnerre, fit trembler la
cabine d'aiguillage.

-Ils sont bien pressés, dit le petit prince. Que cherchent-ils?

-L'homme de la locomotive l'ignore lui-même, dit l'aiguilleur.

Et gronda, en sens inverse, un second rapide illuminé.

-Ils reviennent déjà? demanda le petit prince...

-Ce ne sont pas les mêmes, dit l'aiguilleur. C'est un échange.

-Ils n'étaient pas contents, là où ils étaient?

-On n'est jamais content là où on est, dit l'aiguilleur.

Et gronda le tonnerre d'un troisième rapide illuminé.

-Ils poursuivent les premiers voyageur demanda le petit prince.

-Ils ne poursuivent rien du tout, dit l'aiguilleur. Ils dorment
là-dedans, ou bien ils bâillent. Les enfants seuls écrasent leur nez
contre les vitres.

-Les enfants seuls savent ce qu'ils cherchent, fit le petit prince.
Ils perdent du temps pour une poupée de chiffons, et elle devient très
importante, et si on la leur enlève, ils pleurent...

-Ils ont de la chance, dit l'aiguilleur.
