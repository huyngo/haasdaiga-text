# CHAPITRE XXIV

Nous en étions au huitième jour de ma panne dans le désert, et j'avais
écouté l'histoire du marchand en buvant la dernière goutte de ma
provision d'eau:

-Ah! dis-je au petit prince, ils sont bien jolis, tes souvenirs, mais
je n'ai pas encore réparé mon avion, je n'ai plus rien à boire, et je
serais heureux, moi aussi, si je pouvais marcher tout doucement vers
une fontaine!

-Mon ami le renard, me dit-il...

-Mon petit bonhomme, il ne s'agit plus du renard!

-Pourquoi?

-Parce qu'on va mourir de soif...

Il ne comprit pas mon raisonnement, il me répondit:

-Ç'est bien d'avoir eu un ami, même si l'on va mourir. Moi, je suis
bien content d'avoir eu un ami renard...

Il ne mesure pas le danger, me dis-je. Il n'a jamais ni faim ni soif.
Un peu de soleil lui suffit...

Mais il me regarda et répondit à ma pensée:

-J'ai soif aussi...cherchons un puits...

J'eus un geste de lassitude: il est absurde de chercher un puits, au
hasard, dans l'immensité du désert. Cependant nous nous mîmes en
marche.

Quand nous eûmes marché, des heures, en silence, la nuit tomba, et les
étoiles commencèrent de s'éclairer. Je les apercevais comme dans un
rêve, ayant un peu de fièvre, à cause de ma soif. Les mots du petit
prince dansaient dans ma mémoire:

-Tu as donc soif aussi? lui demandai-je.

Mais il ne répondit pas à ma question. Il me dit simplement:

-L'eau peut aussi être bonne pour le coeur...

Je ne compris pas sa réponse mais je me tus...Je savais bien qu'il ne
fallait pas l'interroger.

Il était fatigué. Il s'assit. Je m'assis auprès de lui. Et, après un
silence, il dit encore:

-Les étoiles sont belles, à cause d'une fleur que l'on ne voit pas...

Je répondis "bien sûr" et je regardai, sans parler, les plis du sable
sous la lune.

-Le désert est beau, ajouta-t-il...

Et c'était vrai. J'ai toujours aimé le désert. On s'assoit sur une
dune de sable. On ne voit rien. On n'entend rien. Et cependant quelque
chose rayonne en silence...

-Ce qui embellit le désert, dit le petit prince, c'est qu'il cache un
puits quelque part...

Je fus surpris de comprendre soudain ce mystérieux rayonnement du
sable.  Lorsque j'étais petit garçon j'habitais une maison ancienne,
et la légende racontait qu'un trésor y était enfoui. Bien sûr, jamais
personne n'a su le découvrir, ni peut-être même ne l'a cherché. Mais
il enchantait toute cette maison. Ma maison cachait un secret au fond
de son coeur...

-Oui, dis-je au petit prince, qu'il s'agisse de la maison, des étoiles
ou du désert, ce qui fait leur beauté est invisible!

-Je suis content, dit-il, que tu sois d'accord avec mon renard.

Comme le petit prince s'endormait, je le pris dans mes bras, et me
remis en route. J'étais ému. Il me semblait porter un trésor fragile.
Il me semblait même qu'il n'y eût rien de plus fragile sur la Terre.
Je regardais, à la lumière de la lune, ce front pâle, ces yeux clos,
ces mèches de cheveux qui tremblaient au vent, et je me disais: ce que
je vois là n'est qu'une écorce. Le plus important est invisible...

Comme ses lèvres entr'ouvertes ébauchaient un demi-sourire je me dis
encore: "Ce qui m'émeut si fort de ce petit prince endormi, c'est sa
fidélité pour une fleur, c'est l'image d'une rose qui rayonne en lui
comme la flamme d'une lampe, même quand il dort..." Et je le devinai
plus fragile encore. Il faut bien protéger les lampes: un coup de vent
peut les éteindre...

Et, marchant ainsi, je découvris le puits au lever du jour.
