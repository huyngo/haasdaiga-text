# CHAPITRE XV

La sixième planète était une planète dix fois plus vaste. Elle était
habitée par un vieux Monsieur qui écrivait d'énormes livres.

![la planète de l'explorateur](http://gutenberg.net.au/ebooks03/0300771h-images/0300771h-29.jpg)

-Tiens! voilà un explorateur! s'écria-t-il, quand il aperçut le petit
prince.

Le petit prince s'assit sur la table et souffla un peu. Il avait déjà
tant voyagé!

-D'où viens-tu? lui dit le vieux Monsieur.

-Quel est ce gros livre? dit le petit prince. Que faites-vous ici?

-Je suis géographe, dit le vieux Monsieur.

-Qu'est-ce un géographe?

-C'est un savant qui connaît où se trouvent les mers, les fleuves, les
villes, les montagnes et les déserts.

-Ça c'est bien intéressant, dit le petit prince. Ça c'est enfin un
véritable métier! Et il jeta un coup d'oeil autour de lui sur la
planète du géographe. Il n'avait jamais vu encore une planète aussi
majestueuse.

-Elle est bien belle, votre planète. Est-ce qu'il y a des océans?

-Je ne puis pas le savoir, dit le géographe.

-Ah! (Le petit prince était déçu.) Et des montagnes?

-Je ne puis pas le savoir, dit le géographe.

-Et des villes et des fleuves et des déserts?

-Je ne puis pas le savoir non plus, dit le géographe.

-Mais vous êtes géographe!

-C'est exact, dit le géographe, mais je ne suis pas explorateur. Je
manque absolument d'explorateurs. Ce n'est pas le géographe qui va
faire le compte des villes, des fleuves, des montagnes, des mers et
des océans. La géographe est trop important pour flâner. Il ne quitte
pas son bureau. Mais il reçoit les explorateurs. Il les interroge, et
il prend note leurs souvenirs. Et si les souvenirs de l'un d'entre eux
lui paraissent intéressants, le géographe fait une enquête sur la
moralité de l'explorateur.

-Pourquoi ça?

-Parce qu'un explorateur qui mentait entraînerait des catastrophes
dans les livres de géographie. Et aussi un explorateur qui boirait
trop.

-Pourquoi ça? fit le petit prince.

-Parce que les ivrognes voient double. Alors le géographe noterait
deux montagnes, là où il n'y en a qu'un seule.

-Je connais quelqu'un, dit le petit prince, qui serait mauvais
explorateur.

-C'est possible. Donc, quand la moralité de l'explorateur paraît
bonne, on fait une enquête sur sa découverte.

-On va voir?

-Non. C'est trop compliqué. Mais on exige de l'explorateur qu'il
fournisse de preuves. Si'il s'agit par example de la découverte d'une
grosse montagne, on exige qu'il en rapporte de grosses pierres.

Le géographe soudain s'émut.

-Mais toi, tu viens de loin! Tu es explorateur! Tu vas me décrire ta
planète!

Et le géographe, ayant ouvert son registre, tailla son crayon. On note
d'abord au crayon les récits des explorateurs. On attend, pour noter à
l'encre, que l'explorateur ait fourni des preuves.

-Alors? interrogea le géographe.

-Oh! chez moi, dit le petit prince, ce n'est pas très intéressant,
c'est tout petit. J'ai trois volcans. Deux volcans en activité, et un
volcan éteint. Mais on ne sait jamais.

-On ne sait jamais, dit le géographe.

-J'ai aussi une fleur.

-Nous ne notons pas les fleurs, dit le géographe.

-Pourquoi ça! c'est le plus joli!

-Parce que les fleurs sont éphémères.

-Qu'est ce que signifie: "éphémère"?

-Les géographies, dit le géographe, sont les livres les plus précieux
de tous les livres. Elles ne se démodent jamais. Il est rare qu'une
montagne change de place. Il est très rare qu'un océan se vide de son
eau. Nous écrivons des choses éternelles.

-Mais les volcans éteints peuvent se réveiller, interrompit le petit
prince. Qu'est -ce que signifie "éphémère"?

-Que les volcans soient éteints ou soient éveillés, ça revient au même
pour nous autres, dit le géographe. Ce qui compte pour nous, c'est la
montagne. Elle ne change pas.

-Mais qu'est-ce que signifie "éphémère"? répéta le petit prince qui,
de sa vie, n'avait renoncé à une question, une fois qu'il l'avait
posée.

-Ça signifie "qui est menacé de disparition prochaine".

-Ma fleur est menacée de disparition prochaine?

-Bien sûr.

Ma fleur est éphémère, se dit le petit prince, et elle n'a que quatre
épines pour se défendre contre le monde! Et je l'ai laissée toute
seule chez moi!

Ce fut là son premier mouvement de regret. Mais il reprit courage:

-Que me conseillez-vous d'aller visiter? demanda-t-il.

-La planète Terre, lui répondit le géographe. Elle a une bonne
réputation...

Et le petit prince s'en fut, songeant à sa fleur.
