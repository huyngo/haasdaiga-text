# CHAPITRE VI

Ah! petit prince, j'ai compris, peu à peu, ainsi, ta petite vie
mélancolique. Tu n'avais eu longtemps pour ta distraction que la douceur
des couchers du soleil. J'ai appris ce détail nouveau, le quatrième jour au
matin, quand tu m'as dit:

-J'aime bien les couchers de soleil. Allons voir un coucher de soleil...

-Mais il faut attendre...

-Attendre quoi?

-Attendre que le soleil se couche.

Tu as eu l'air très surpris d'abord, et puis tu as ri de toi-même. Et tu
m'as dit:

-Je me crois toujours chez moi!

En effet. Quand il est midi aux Etats-Unis, le soleil, tout le monde sait,
se couche sur la France. Il suffirait de pouvoir aller en France en une
minute pour assister au coucher de soleil. Malheureusement la France est
bien trop éloignée. Mais, sur ta si petite planète, il te suffirait de
tirer ta chaise de quelques pas. Et tu regardais le crépuscule chaque fois
que tu le désirais...

-Un jour, j'ai vu le soleil se coucher quarante-trois fois!

Et un peu plus tard tu ajoutais:

-Tu sais...quand on est tellement triste on aime les couchers de soleil...

-Le jour des quarante-trois fois tu étais donc tellement triste? Mais le
petit prince ne répondit pas.
