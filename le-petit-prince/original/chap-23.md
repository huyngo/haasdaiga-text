# CHAPITRE XXIII

-Bonjour, dit le petit prince.

-Bonjour, dit le marchand de pilules perfectionnées qui apaisent la
soif. On en avale une par semaine et l'on n'éprouve plus le besoin de
boire.

-Pourquoi vends-tu ça? dit le petit prince.

-C'est une grosse économie de temps, dit le marchand. Les experts ont
fait des calculs. On épargne cinquante-trois minutes par semaine.

-Et que fait-on des cinquante-trois minutes?

-On fait ce que l'on veut...

"Moi, se dit le petit prince, si j'avais cinquante-trois minutes à
dépenser, je marcherais tout doucement vers une fontaine..."
