# CHAPITRE XVIII

Le petit prince traversa le désert et ne rencontra qu'une fleur. Une
fleur à trois pétales, une fleur de rien du tout...

-Bonjour, dit le petit prince.

-Bonjour, dit la fleur.

-Où sont les hommes? demanda poliment le petit prince.

La fleur, un jour, avait vu passer une caravane:

![la
fleur](http://gutenberg.net.au/ebooks03/0300771h-images/0300771h-32.jpg)

-Les hommes? Il en existe, je crois, six ou sept. Je les ai aperçus il
y a des années. Mais on ne sait jamais où les trouver. Le vent les
promène. Ils manquent de racines, ça les gêne beaucoup.

-Adieu, fit le petit prince.

-Adieu, dit la fleur.
