# Text information

- Source language: German
- Retrieved from: [Grimm's Fairy Tales][grimm], with English translation
- Original work status: Public domain

[grimm]: https://www.grimmstories.com/language.php?grimm=015&l=en&r=de
